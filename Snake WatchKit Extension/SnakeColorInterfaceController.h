//
//  SnakeColorInterfaceController.h
//  WatchSnake
//
//  Created by Sameh Mabrouk on 5/26/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

#import <WatchKit/WatchKit.h>
#import <Foundation/Foundation.h>

@interface SnakeColorInterfaceController : WKInterfaceController

@end
