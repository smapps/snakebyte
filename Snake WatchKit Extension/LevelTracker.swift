//
//  LevelTracker.swift
//  Snake
//
//  Created by Sameh Mabrouk on 9/3/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

import Foundation

struct LevelTracker {

    static var highestUnlockedLevel = 0
    var currentLevel = 0

    init(highestUnlockedLevel: Int, currentLevel: Int){

        LevelTracker.highestUnlockedLevel = highestUnlockedLevel
        self.currentLevel = currentLevel
        
    }

    static func unlockLevel(level: Int){

        if level > highestUnlockedLevel {

            highestUnlockedLevel = level
        }
    }

    static func levelIsUnlocked(level: Int) -> Bool {

        return level <= highestUnlockedLevel

    }


    mutating func advanceToLevel(level: Int) -> Bool {

        if LevelTracker.levelIsUnlocked(level) {

            currentLevel = level

            return true
        } else {

            return false
        }

    }
    
}