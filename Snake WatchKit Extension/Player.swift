//
//  Player.swift
//  Snake
//
//  Created by Sameh Mabrouk on 9/3/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

import Foundation

class Player {

    var tracker = LevelTracker(highestUnlockedLevel: 0, currentLevel: 0)

    let playerName:String

    func completedLevel(level: Int){

        LevelTracker.unlockLevel(level + 1)

        tracker.advanceToLevel(level + 1)

    }

    init(name: String){

        playerName = name
        
    }
    
}