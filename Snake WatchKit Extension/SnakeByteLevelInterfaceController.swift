//
//  SnakeByteLevelInterfaceController.swift
//  Snake
//
//  Created by Sameh Mabrouk on 7/19/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

import WatchKit

class SnakeByteLevelInterfaceController: WKInterfaceController {


    @IBOutlet weak var minionTable: WKInterfaceTable!

    let minions = TableDataSource().levels

    override func awakeWithContext(context: AnyObject?) {
        super.awakeWithContext(context)

        loadTableData()
    }

    private func loadTableData() {

        minionTable.setNumberOfRows(minions.count, withRowType: "LevelTableRowController")

        for (index, minionName) in minions.enumerate() {

            let row = minionTable.rowControllerAtIndex(index) as! TableRowController

            row.interfaceLabel.setText(minionName)

            row.interfaceImage.setImageNamed(minionName.lowercaseString)
        }

    }

    override func contextForSegueWithIdentifier(segueIdentifier: String, inTable table: WKInterfaceTable, rowIndex: Int) -> AnyObject?
    {

        let minionName = minions[rowIndex]
        return minionName
    }

    override func table(table: WKInterfaceTable,
        didSelectRowAtIndex rowIndex: Int){

            print("didSelectRowAtIndex")
            /*
            //Notify InterfaceController that snake color changed.
            NSUserDefaults.standardUserDefaults().setObject(minions[rowIndex].lowercaseString, forKey: "Color")
            self.dismissController()
            */
            print("selected Color \(minions[rowIndex])")

            if NSUserDefaults.standardUserDefaults().objectForKey("Color") == nil{

                NSUserDefaults.standardUserDefaults().setObject(self.minions[rowIndex].lowercaseString, forKey: "Color")
                NSNotificationCenter.defaultCenter().postNotificationName("SnakeColorChanged", object: nil, userInfo: nil)

            }

            let str: String = NSUserDefaults.standardUserDefaults().objectForKey("Color")! as! String
            print("Saved Color \(str)")

            if minions[rowIndex].lowercaseString != str{
                print("not equal")
                NSUserDefaults.standardUserDefaults().setObject(minions[rowIndex].lowercaseString, forKey: "Color")
                NSUserDefaults.standardUserDefaults().synchronize()
                NSNotificationCenter.defaultCenter().postNotificationName("SnakeColorChanged", object: nil, userInfo: nil)

            }
            print("Final Saved Color \(str)")


            self.dismissController()

    }
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        NSLog("%@ will activate", self)
    }

    override func didDeactivate() {
        // This method is called when watch view controller is no longer visible
        NSLog("%@ did deactivate", self)
        super.didDeactivate()
    }

}
