//
//  TableDataSource.swift
//  Snake
//
//  Created by Sameh Mabrouk on 5/29/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//


class TableDataSource: NSObject {
    
    let minions = ["Red", "Green", "Blue", "White"]
    let levels = ["Level 1", "Level 2", "Level 3", "Level 4"]
}
