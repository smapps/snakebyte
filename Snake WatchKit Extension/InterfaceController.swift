//
//  InterfaceController.swift
//  Snake WatchKit Extension
//
//  Created by Sameh Mabrouk on 5/29/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

import WatchKit
import Foundation

//SnakeByte Constants Properties.
let SnakeByteDeadGrassColor = UIColor(red: 255.0/255, green: 101.0/255, blue: 89.0/255, alpha: 1.0).CGColor
let SnakeByteColor = UIColor.whiteColor().CGColor
let SnakByteAppleColor = UIColor.greenColor().CGColor
let SnakeByteLevelColor = UIColor.redColor().CGColor

//Different snake colors.
let SnakByteWhiteColor = UIColor.whiteColor().CGColor
let SnakByteRedColor = UIColor(red: 255.0/255, green: 101.0/255, blue: 89.0/255, alpha: 1.0).CGColor
let SnakByteGreenColor = UIColor(red: 154.0/255, green: 242.0/255, blue: 49.0/255, alpha: 1.0).CGColor
let SnakByteBlueColor = UIColor(red: 49.0/255, green: 188.0/255, blue: 250.0/255, alpha: 1.0).CGColor

//Levels target score.
let level1TargetScore = 2
let level2TargetScore = 3
let level3TargetScore = 30
let level4TargetScore = 40

func DCCGPointIsNearPoint(p1:CGPoint, p2:CGPoint) -> Bool {
    let xDiff = p1.x - p2.x
    let yDiff = p1.y - p2.y
    return xDiff * xDiff + yDiff * yDiff < 1.0
}

enum Direction : Int {
    case Up = 0
    case Right
    case Down
    case Left
    
    func xDiff() -> Int {
        switch(self) {
        case .Up:
            return 0
        case .Right:
            return 1
        case .Down:
            return 0
        case .Left:
            return -1
        }
    }
    func yDiff() -> Int {
        switch(self) {
        case .Up:
            return 1
        case .Right:
            return 0
        case .Down:
            return -1
        case .Left:
            return 0
        }
    }
    
    func nextValueClockwise() -> Direction {
        switch(self) {
        case .Up:
            return .Right
        case .Right:
            return .Down
        case .Down:
            return .Left
        case .Left:
            return .Up
        }
    }
    func nextValueCounterClockwise() -> Direction {
        switch(self) {
        case .Up:
            return .Left
        case .Right:
            return .Up
        case .Down:
            return .Right
        case .Left:
            return .Down
        }
    }
    
    func isHorizontal() -> Bool {
        switch(self) {
        case .Up:
            return false
        case .Right:
            return true
        case .Down:
            return false
        case .Left:
            return true
        }
    }
    func isVertical() -> Bool {
        switch(self) {
        case .Up:
            return true
        case .Right:
            return false
        case .Down:
            return true
        case .Left:
            return false
        }
    }
}

struct DiscretePoint : Equatable, CustomStringConvertible {
    let x: Int, y: Int
    
    init(_ x: Int, _ y: Int) {
        self.x = x
        self.y = y
    }
    
    static var Zero: DiscretePoint {
        get {
            return DiscretePoint(0, 0)
        }
    }
    var cgPoint: CGPoint! {
        get {
            return CGPointMake(CGFloat(self.x), CGFloat(self.y))
        }
    }
    var description: String {
        get {
            return "[\(self.x), \(self.y)]"
        }
    }
}
func ==(lhs: DiscretePoint, rhs: DiscretePoint) -> Bool {
    return lhs.x == rhs.x && lhs.y == rhs.y
}

struct DiscreteSize {
    let width: Int, height: Int
    
    static var Zero: DiscreteSize {
        get {
            return DiscreteSize(width: 0, height: 0)
        }
    }
    var cgSize: CGSize! {
        get {
            return CGSizeMake(CGFloat(self.width), CGFloat(self.height))
        }
    }
}
struct DiscreteRect {
    let origin: DiscretePoint
    let size: DiscreteSize
    
    func containsPoint(point: DiscretePoint) -> Bool {
        return point.x >= origin.x && point.y >= origin.y && point.x < (origin.x + size.width) && point.y < (origin.y + size.height)
    }
    
    var cgRect: CGRect! {
        get {
            return CGRect(origin: self.origin.cgPoint, size: self.size.cgSize)
        }
    }
}

class SnakeGame {
    let updateInterval = 0.3
    let gameSize = DiscreteSize(width: 16, height: 16)
    let blockSize:Int = 8
    var imageOutputSize: CGSize {
        get {
            return DiscreteSize(width: self.gameSize.width * self.blockSize, height: self.gameSize.height * self.blockSize).cgSize
        }
    }
    
    let USE_IMAGES = false
    var appleImage: UIImage?
    var snakePartsImage: UIImage?
    let aliveGrassColor = UIColor(red: 51.0/255, green: 51.0/255, blue: 51.0/255, alpha: 0.0).CGColor
    let deadGrassColor = SnakeByteDeadGrassColor
    var snakeColor = SnakeByteColor
    var appleColor = SnakByteAppleColor
    var LevelColor = SnakeByteLevelColor
    
//    var snakeParts = [DiscretePoint(6, 10), DiscretePoint(5, 10), DiscretePoint(4, 10)]
//    var snakeParts = [DiscretePoint(6, 4), DiscretePoint(5, 4), DiscretePoint(4, 4)]
    var snakeParts = [DiscretePoint(3, 4), DiscretePoint(2, 4), DiscretePoint(1, 4)]
    var direction: Direction = .Right
    var pendingDirection: Direction? = nil
    var isAlive = true
    var applesEaten = 0
    var apples = [DiscretePoint]()
    var eatenApplePoints = [DiscretePoint]()
    
    var mySharedDefaults:NSUserDefaults?

    var gameLevels = [Level]()

    var currentGameLevel:Level!

    var level1Parts = [DiscretePoint(10, 9), DiscretePoint(10, 8), DiscretePoint(10, 7), DiscretePoint(10, 6), DiscretePoint(10, 5), DiscretePoint(10, 4), DiscretePoint(10, 3), DiscretePoint(10, 2), DiscretePoint(10, 1), DiscretePoint(10, 0)]
    var level11Parts = [DiscretePoint(5, 15), DiscretePoint(5, 14), DiscretePoint(5, 13), DiscretePoint(5, 12), DiscretePoint(5, 11), DiscretePoint(5, 10), DiscretePoint(5, 9), DiscretePoint(5, 8), DiscretePoint(5, 7), DiscretePoint(5, 6)]


    init(gameLevel level:Int) {
        if USE_IMAGES {
            self.appleImage = UIImage(named: "apple")
            self.snakePartsImage = UIImage(named: "snake_sprites")
        }
        
        //initialize SharedDefaults.
        self.mySharedDefaults = NSUserDefaults(suiteName: "group.com.smapps.SnakeByte")

        //Create game levels.
        self.createGameLevels(level)

    }

    func createGameLevels(level:Int){

        //Create game levels.
        //Level1
        let level1: Level = Level()
        level1.levelScore = level1TargetScore
        level1.levelFirstParts = [DiscretePoint(10, 9), DiscretePoint(10, 8), DiscretePoint(10, 7), DiscretePoint(10, 6), DiscretePoint(10, 5), DiscretePoint(10, 4), DiscretePoint(10, 3), DiscretePoint(10, 2), DiscretePoint(10, 1), DiscretePoint(10, 0)]
        level1.levelSecondParts = [DiscretePoint(5, 15), DiscretePoint(5, 14), DiscretePoint(5, 13), DiscretePoint(5, 12), DiscretePoint(5, 11), DiscretePoint(5, 10), DiscretePoint(5, 9), DiscretePoint(5, 8), DiscretePoint(5, 7), DiscretePoint(5, 6)]
        gameLevels.append(level1)

        //Level2
        let level2: Level = Level()
        level2.levelScore = level2TargetScore
        level2.levelFirstParts = [DiscretePoint(15, 9), DiscretePoint(14, 9), DiscretePoint(13, 9), DiscretePoint(12, 9), DiscretePoint(11, 9), DiscretePoint(10, 9), DiscretePoint(9, 9), DiscretePoint(8, 9), DiscretePoint(7, 9), DiscretePoint(6, 9)]
        level2.levelSecondParts = [DiscretePoint(0, 5), DiscretePoint(1, 5), DiscretePoint(2, 5), DiscretePoint(3, 5), DiscretePoint(4, 5), DiscretePoint(5, 5), DiscretePoint(6, 5), DiscretePoint(7, 5), DiscretePoint(8, 5), DiscretePoint(9, 5)]
        gameLevels.append(level2)

        //Level3
        let level3: Level = Level()
        level3.levelScore = level3TargetScore
        level3.levelFirstParts = [DiscretePoint(13, 10), DiscretePoint(12, 10), DiscretePoint(11, 10), DiscretePoint(10, 10), DiscretePoint(9, 10), DiscretePoint(8, 10), DiscretePoint(7, 10)]
        level3.levelSecondParts = [DiscretePoint(0, 5), DiscretePoint(1, 5), DiscretePoint(2, 5), DiscretePoint(3, 5), DiscretePoint(3, 6), DiscretePoint(3, 7), DiscretePoint(3, 8), DiscretePoint(4, 8), DiscretePoint(5, 8), DiscretePoint(6, 8), DiscretePoint(7, 8), DiscretePoint(8, 8)]
        gameLevels.append(level3)

        //Level4
        let level4: Level = Level()
        level4.levelScore = level4TargetScore
        level4.levelFirstParts = [DiscretePoint(13, 14), DiscretePoint(14, 14), DiscretePoint(15, 14), DiscretePoint(15, 13), DiscretePoint(15, 12), DiscretePoint(15, 11), DiscretePoint(15, 10), DiscretePoint(15, 9), DiscretePoint(15, 8), DiscretePoint(15, 7), DiscretePoint(15, 6), DiscretePoint(15, 5), DiscretePoint(15, 4), DiscretePoint(15, 3), DiscretePoint(15, 2), DiscretePoint(15, 1), DiscretePoint(14, 1), DiscretePoint(13, 1)]

        level4.levelSecondParts = [DiscretePoint(2, 14), DiscretePoint(1, 14), DiscretePoint(0, 14), DiscretePoint(0, 13), DiscretePoint(0, 12), DiscretePoint(0, 11), DiscretePoint(0, 10), DiscretePoint(0, 9), DiscretePoint(0, 8), DiscretePoint(0, 7), DiscretePoint(0, 6), DiscretePoint(0, 5), DiscretePoint(0, 4), DiscretePoint(0, 3), DiscretePoint(0, 2), DiscretePoint(0, 1), DiscretePoint(1, 1), DiscretePoint(2, 1)]
//        level4.levelSecondParts = [DiscretePoint(11, 13), DiscretePoint(11, 12), DiscretePoint(11, 11), DiscretePoint(11, 10), DiscretePoint(11, 9), DiscretePoint(11, 8), DiscretePoint(11, 7), DiscretePoint(11, 6), DiscretePoint(12, 6), DiscretePoint(13, 6), DiscretePoint(10, 6), DiscretePoint(9, 6)]


        gameLevels.append(level4)


        currentGameLevel = gameLevels[level]

    }
    func isValidSnake(snakeParts:[DiscretePoint]) -> Bool {
        for snakePartIdx in 0..<snakeParts.count {
            let snakePart = snakeParts[snakePartIdx]
            // Check for the game boundary intersection.
            if !DiscreteRect(origin: DiscretePoint.Zero, size: self.gameSize).containsPoint(snakePart) {
                return false
            }
            
            // Check for snake self-intersection.
            for otherSnakePart in snakeParts[snakePartIdx + 1 ..< snakeParts.count] {
                if snakePart == otherSnakePart {
                    return false
                }
            }

            // Check for snake intersection with Level blocks.
            // Level first part
            for otherSnakePart in self.currentGameLevel.levelFirstParts[snakePartIdx + 1 ..< self.self.currentGameLevel.levelFirstParts.count] {
                if snakePart == otherSnakePart {
                    return false
                }
            }

            // Level second part
            for otherSnakePart in self.currentGameLevel.levelSecondParts[snakePartIdx + 1 ..< self.self.currentGameLevel.levelSecondParts.count] {
                if snakePart == otherSnakePart {
                    return false
                }
            }

        }
        
        // Everything looks good!
        return true
    }
    
    func isValidApplePoint(applePoint: DiscretePoint) -> Bool {
        for snakePart in self.snakeParts {
            if applePoint == snakePart {
                return false
            }
        }
        return true
    }
    func newApplePoint() -> DiscretePoint {
        // Tail recursion like this is totally reasonable in FP; hopefully Swift actually implements it correctly!
        let applePoint = DiscretePoint(random() % self.gameSize.width, random() % self.gameSize.height)
        return (isValidApplePoint(applePoint) ? applePoint : newApplePoint())
    }
    
    func advanceSnakeParts(snakeParts:[DiscretePoint], eatenApplePoints:[DiscretePoint]) -> ([DiscretePoint], [DiscretePoint]) {
        // Move the first snake-part forward.
        var newSnakeParts = [DiscretePoint]()
        newSnakeParts.append(DiscretePoint(snakeParts[0].x + self.direction.xDiff(), snakeParts[0].y + self.direction.yDiff()))
        
        // Move the rest of the snake parts forward.
        let srcRange:Range = 0...snakeParts.count - 2
        newSnakeParts.appendContentsOf(snakeParts[srcRange])
        
        // See if the end of the original snake is an eaten apple point!
        var newEatenApplePoints = eatenApplePoints
        for eatenAppleIdx in 0..<eatenApplePoints.count {
            let eatenApple = eatenApplePoints[eatenAppleIdx]
            if snakeParts.last! == eatenApple {
                newEatenApplePoints.removeAtIndex(eatenAppleIdx)
                newSnakeParts.append(eatenApple)
                break
            }
        }
        return (newSnakeParts, newEatenApplePoints)
    }
    func update() {
        // If we're already dead, just bail!
        if !self.isAlive {
            
            //Save High Score(number of apple eaten) before game over.
            if  mySharedDefaults?.integerForKey("HighScore") == nil{
                mySharedDefaults?.setInteger(self.applesEaten, forKey: "HighScore")
            }
            else{
                let savedHighScore = mySharedDefaults?.integerForKey("HighScore")
                if self.applesEaten > savedHighScore{
                    mySharedDefaults?.setInteger(self.applesEaten, forKey: "HighScore")
                }
                
            }

            let savedHS = mySharedDefaults?.integerForKey("HighScore")
        
            return
        }
        
        // Let the snake change direction!
        if let newDirection = self.pendingDirection {
            self.direction = newDirection
            self.pendingDirection = nil
        }
        
        // Move the snake forward!
        (self.snakeParts, self.eatenApplePoints) = self.advanceSnakeParts(self.snakeParts, eatenApplePoints:self.eatenApplePoints)
        
        // See if we should add an apple.
        if 0 == random() % 20 || 0 == self.apples.count {
            let applePoint = self.newApplePoint()
            self.apples.append(applePoint)
        }
        
        // See if the snake eats an apple!
        for appleIdx in 0..<self.apples.count {
            let apple = self.apples[appleIdx]
            if self.snakeParts[0] == apple {
                self.eatenApplePoints.append(apple)
                self.apples.removeAtIndex(appleIdx)
                
                self.applesEaten++
                
                
                break
            }
        }
        
        // Check for DEATH!
        self.isAlive = isValidSnake(self.snakeParts)
//        self.isAlive = isValidSnake(self.level1Parts)
    }
    
    func directionFromPoint(point: DiscretePoint, toPoint otherPoint: DiscretePoint) -> Direction {
        let yDiff = otherPoint.y - point.y
        if abs(yDiff) > 0 {
            // There is a change in y!
            if yDiff > 0 {
                return .Up
            }
            else if yDiff < 0 {
                return .Down
            }
        }
        
        let xDiff = otherPoint.x - point.x
        if abs(xDiff) > 0 {
            // There is a change in x!
            if xDiff > 0 {
                return .Right
            }
            else if xDiff < 0 {
                return .Left
            }
        }
        
        // Who knows! Let's return Up, I guess!
        return .Up
    }
    
    func rectForGamePoint(gamePoint: DiscretePoint) -> CGRect {
        return DiscreteRect(origin: DiscretePoint(self.blockSize * gamePoint.x, self.blockSize * gamePoint.y), size: DiscreteSize(width: self.blockSize, height: self.blockSize)).cgRect
    }
    
    func imageSourceOriginForMiddleSnakePart(index snakePartIdx: Int) -> CGPoint {
        let snakePart = self.snakeParts[snakePartIdx]
        let leadingSnakePart = self.snakeParts[snakePartIdx - 1]
        let trailingSnakePart = self.snakeParts[snakePartIdx + 1]
        
        var hasUp = false
        var hasDown = false
        var hasRight = false
        var hasLeft = false
        switch self.directionFromPoint(snakePart, toPoint: leadingSnakePart) {
        case .Up:
            hasUp = true
        case .Right:
            hasRight = true
        case .Down:
            hasDown = true
        case .Left:
            hasLeft = true
        }
        switch self.directionFromPoint(snakePart, toPoint: trailingSnakePart) {
        case .Up:
            hasUp = true
        case .Right:
            hasRight = true
        case .Down:
            hasDown = true
        case .Left:
            hasLeft = true
        }
        
        var columnOffset = 0
        var rowOffset = 0
        if hasUp && hasDown {
            // Totally vertical!
            columnOffset = 0
            rowOffset = (random() % 5) + 1
        }
        else if hasLeft && hasRight {
            // Totally horizontal!
            columnOffset = 1
            rowOffset = (random() % 5) + 1
        }
        else if hasDown {
            rowOffset = 1
            columnOffset = (hasRight ? 2 : 3)
        }
        else {
            rowOffset = 2
            columnOffset = (hasRight ? 2 : 3)
        }
        
        return CGPointMake(64.0 * CGFloat(columnOffset), 64.0 * CGFloat(rowOffset))
    }
    func drawSnakeSegment(snakePartRect: CGRect, fromSourceOrigin sourceOrigin: CGPoint, inContext context: CGContextRef) {
        let snakePartImageRect = CGRectMake(sourceOrigin.x, sourceOrigin.y, 64.0, 64.0)
        let snakePartImage = CGImageCreateWithImageInRect(self.snakePartsImage?.CGImage, snakePartImageRect)
        CGContextDrawImage(context, snakePartRect, snakePartImage)
    }
    func drawFlatSnakeSegment(snakePartRect: CGRect, inContext context: CGContextRef) {
        CGContextSaveGState(context)
        CGContextSetFillColorWithColor(context, self.snakeColor)
        CGContextFillRect(context, snakePartRect)
        CGContextRestoreGState(context)
    }
    func drawLevelSegment(snakePartRect: CGRect, inContext context: CGContextRef) {
        CGContextSaveGState(context)
        CGContextSetFillColorWithColor(context, self.LevelColor)
        CGContextFillRect(context, snakePartRect)
        CGContextRestoreGState(context)
    }
    func renderSnake(inContext context:CGContextRef) {

        // First draw the head.
        if USE_IMAGES {
            drawSnakeSegment(rectForGamePoint(self.snakeParts[0]), fromSourceOrigin: CGPointMake(64.0 * CGFloat(self.direction.rawValue), 0.0), inContext: context)
        }
        else {
            drawFlatSnakeSegment(rectForGamePoint(self.snakeParts[0]), inContext: context)
        }
        
        // Now draw the tail.
        if USE_IMAGES {
            let tailDirection = self.directionFromPoint(self.snakeParts.last!, toPoint: self.snakeParts[self.snakeParts.count - 2])
            drawSnakeSegment(rectForGamePoint(self.snakeParts.last!), fromSourceOrigin: CGPointMake(64.0 * CGFloat(tailDirection.rawValue), 64.0 * 6.0), inContext: context)
        }
        else {
            drawFlatSnakeSegment(rectForGamePoint(self.snakeParts.last!), inContext: context)
        }

        // Lastly, draw the middle bits!
        for snakePartIdx in 1 ... self.snakeParts.count - 2 {
            let snakePart = self.snakeParts[snakePartIdx]

            if USE_IMAGES {
                let imageSourceOrigin = self.imageSourceOriginForMiddleSnakePart(index: snakePartIdx)
                drawSnakeSegment(rectForGamePoint(snakePart), fromSourceOrigin: imageSourceOrigin, inContext: context)
            }
            else {
                drawFlatSnakeSegment(rectForGamePoint(snakePart), inContext: context)
            }
        }

        // Draw Levels.
        //Draw Level First Part.
        for snakePartIdx in 0 ... self.currentGameLevel.levelFirstParts.count - 1 {
            let snakePart = self.self.currentGameLevel.levelFirstParts[snakePartIdx]
//            let levelPart = self.self.currentGameLevel.levelSecondParts[snakePartIdx]
            //            println("snakePart \(snakePart)")

            if USE_IMAGES {
                let imageSourceOrigin = self.imageSourceOriginForMiddleSnakePart(index: snakePartIdx)
                drawSnakeSegment(rectForGamePoint(snakePart), fromSourceOrigin: imageSourceOrigin, inContext: context)
            }
            else {
                drawLevelSegment(rectForGamePoint(snakePart), inContext: context)
//                drawLevelSegment(rectForGamePoint(levelPart), inContext: context)

            }
        }

        //Draw level second part.
        for snakePartIdx in 0 ... self.currentGameLevel.levelSecondParts.count - 1 {
            let levelPart = self.self.currentGameLevel.levelSecondParts[snakePartIdx]
            drawLevelSegment(rectForGamePoint(levelPart), inContext: context)
        }


        /*
//        println("level1Parts \(self.level1Parts)")
        for snakePartIdx in 0 ... self.level1Parts.count - 1 {
            let snakePart = self.level1Parts[snakePartIdx]
            let levelPart = self.level11Parts[snakePartIdx]
//            println("snakePart \(snakePart)")

            if USE_IMAGES {
                let imageSourceOrigin = self.imageSourceOriginForMiddleSnakePart(index: snakePartIdx)
                drawSnakeSegment(rectForGamePoint(snakePart), fromSourceOrigin: imageSourceOrigin, inContext: context)
            }
            else {
                drawLevelSegment(rectForGamePoint(snakePart), inContext: context)
                drawLevelSegment(rectForGamePoint(levelPart), inContext: context)

            }
        }
*/


    }
    
    func drawApple(point point: DiscretePoint, inContext context: CGContextRef) {
        let appleRect = self.rectForGamePoint(point)
        
        if USE_IMAGES {
            CGContextDrawImage(context, appleRect, self.appleImage?.CGImage)
        }
        else {
            CGContextSaveGState(context)
            CGContextSetFillColorWithColor(context, self.appleColor)
            CGContextFillRect(context, appleRect)
            CGContextRestoreGState(context)
        }
    }
    func render() -> (UIImage) {
        UIGraphicsBeginImageContext(self.imageOutputSize)
        let context = UIGraphicsGetCurrentContext()
        // Flip the y-axis.
        CGContextTranslateCTM(context, 0.0, self.imageOutputSize.height)
        CGContextScaleCTM(context, 1.0, -1.0)
        
        if isAlive {
            // Fill the background!
            CGContextSetFillColorWithColor(context, self.aliveGrassColor)
        }
        else {
            // Fill the background with RED/SnakeByteDeadGrassColor!
            CGContextSetFillColorWithColor(context, self.deadGrassColor)
        }
        CGContextFillRect(context, CGRectMake(0.0, 0.0, self.imageOutputSize.width, self.imageOutputSize.height))
        
        // Draw the apples!
        for apple in self.apples {
            drawApple(point: apple, inContext: context!)
        }
        
        // Draw the snake!
        self.renderSnake(inContext: context!)
        
        let fullImage = UIGraphicsGetImageFromCurrentImageContext()
        UIGraphicsEndImageContext()
        
        // Split up the image into left- and right-halves.
//        let halfWidth = self.imageOutputSize.width / 2.0
//        let leftImage = UIImage(CGImage: CGImageCreateWithImageInRect(fullImage.CGImage, CGRectMake(0.0, 0.0, halfWidth, self.imageOutputSize.height))!)
//        let rightImage = UIImage(CGImage: CGImageCreateWithImageInRect(fullImage.CGImage, CGRectMake(halfWidth, 0.0, halfWidth, self.imageOutputSize.height))!)
        return (fullImage!)
    }
    
    func changeDirectionClockwise() {
        self.pendingDirection = self.direction.nextValueClockwise()
    }
    func changeDirectionCounterClockwise() {
        self.pendingDirection = self.direction.nextValueCounterClockwise()
    }
    func setSnakeColor(){
    
        if NSUserDefaults.standardUserDefaults().objectForKey("Color") != nil{
            
            let str: String = NSUserDefaults.standardUserDefaults().objectForKey("Color")! as! String
            switch str {
                
            case "red":
                self.snakeColor=SnakByteRedColor
            case "green":
                self.snakeColor=SnakByteGreenColor
                self.appleColor=SnakByteRedColor
            case "blue":
                self.snakeColor=SnakByteBlueColor
            case "white":
                self.snakeColor=SnakByteWhiteColor
            default:
                self.snakeColor=SnakByteWhiteColor
                
            }
            
            
        }
    }
}


class InterfaceController: WKInterfaceController {
    @IBOutlet var scoreLabel: WKInterfaceLabel?
    @IBOutlet var timerLabel: WKInterfaceLabel?
    @IBOutlet var leftImageButton: WKInterfaceButton?
    @IBOutlet var rightImageButton: WKInterfaceButton?
    @IBOutlet var gameGroup: WKInterfaceGroup?
    
    var timer: NSTimer?
    var game: SnakeGame
    var selectedGameLevel: Int!

    var player: Player?
    
    override init() {
        self.selectedGameLevel = 0
        self.game = SnakeGame(gameLevel: selectedGameLevel)
        self.game.setSnakeColor()
        self.player = Player(name: "")
    }
    
    override func willActivate() {
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
        //Listen to SnakeColorChanged notification.
        NSNotificationCenter.defaultCenter().addObserver(self, selector: Selector("restartGameWithNewSnakeColor"), name: "SnakeColorChanged", object: nil)
        
        self.timer = NSTimer.scheduledTimerWithTimeInterval(self.game.updateInterval, target: self, selector: Selector("gameTimerFired"), userInfo: nil, repeats: true)

    }

    override func didDeactivate() {
        self.timer?.invalidate()
        
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
    
    @IBAction func leftButtonTapped(sender:AnyObject) {
        if(self.game.isAlive) {
            self.game.changeDirectionCounterClockwise()
        }
        else {
            // Restart!
            self.restartGameWithNewSnakeColor()
        }
    }
    @IBAction func rightButtonTapped(sender:AnyObject) {
        if(self.game.isAlive) {
            self.game.changeDirectionClockwise()
        }
        else {
            // Restart!
            self.restartGameWithNewSnakeColor()
        }
    }
    @IBAction func resumeMenuItemAction(sender:AnyObject) {
        
    }
    @IBAction func restartMenuItemAction(sender:AnyObject) {
        // Restart Game!
        self.restartGameWithNewSnakeColor()
    }
    @IBAction func colorMenuItemAction(sender:AnyObject) {
        self.timer?.invalidate()
        
        self.presentControllerWithName("SnakeByteColorInterface", context: nil)
    }
    @IBAction func levelMenuItemAction(sender:AnyObject) {
        self.timer?.invalidate()

        self.presentControllerWithName("SnakeByteLevel", context: nil)
    }

    func gameTimerFired() {

        //update game HighScore.
        self.game.update()
        
        self.setTitle("Score:\(self.game.applesEaten)")
        
        // setup the gameGroup image!
        let (fullImage) = self.game.render()
        self.gameGroup?.setBackgroundImage(fullImage)


     /*
        if !self.game.isAlive{
        
            self.timer?.invalidate()
        }
        */
    }
    
    func restartGameWithNewSnakeColor() {
/*
        if NSUserDefaults.standardUserDefaults().objectForKey("Color") != nil{
            
            self.game = SnakeGame(gameLevel: 0)
            
            let str: String = NSUserDefaults.standardUserDefaults().objectForKey("Color")! as! String
            switch str {
                
            case "red":
                self.game.snakeColor=SnakByteRedColor
            case "green":
                self.game.snakeColor=SnakByteGreenColor
                self.game.appleColor=SnakByteRedColor
            case "blue":
                self.game.snakeColor=SnakByteBlueColor
            case "white":
                self.game.snakeColor=SnakByteWhiteColor
            default:
                self.game.snakeColor=SnakByteWhiteColor
                
            }
            
            
        }
*/
            // Restart!

            //Check if the current level is ended by knowing that the number of applesEaten larger or equal to levelTargetScore.
            //If so Then move to the next level, else stay on the same level.
            print("Apple eaten:\(self.game.applesEaten)")
            print("level score:\(self.game.currentGameLevel.levelScore)")

            if self.game.currentGameLevel.levelScore == level1TargetScore && self.game.applesEaten >= level1TargetScore {

                //advance player level.
                player?.completedLevel(selectedGameLevel)

//                selectedGameLevel = 1
                selectedGameLevel = player?.tracker.currentLevel
                self.game = SnakeGame(gameLevel: selectedGameLevel)
            }
            else if self.game.currentGameLevel.levelScore == level2TargetScore && self.game.applesEaten >= level2TargetScore {
                print("go to level 3")

                //advance player level.
                player?.completedLevel(selectedGameLevel)

//                selectedGameLevel = 2
                selectedGameLevel = player?.tracker.currentLevel
                self.game = SnakeGame(gameLevel: selectedGameLevel)
            }
            else if self.game.currentGameLevel.levelScore == level3TargetScore && self.game.applesEaten >= level3TargetScore {

                //advance player level.
                player?.completedLevel(selectedGameLevel)

//                selectedGameLevel = 3
                selectedGameLevel = player?.tracker.currentLevel
                self.game = SnakeGame(gameLevel: selectedGameLevel)
            }
/*
            else if self.game.currentGameLevel.levelScore == level4TargetScore && self.game.applesEaten >= level4TargetScore {

                selectedGameLevel = 0
                self.game = SnakeGame(gameLevel: selectedGameLevel)
            }
*/
            else{
                self.game = SnakeGame(gameLevel: selectedGameLevel)
            }


        }
    
}
