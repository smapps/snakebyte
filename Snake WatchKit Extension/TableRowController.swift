//
//  TableRowController.swift
//  Snake
//
//  Created by Sameh Mabrouk on 5/29/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

import WatchKit

class TableRowController: NSObject {
    @IBOutlet weak var interfaceImage: WKInterfaceImage!
    
    @IBOutlet weak var interfaceLabel: WKInterfaceLabel!
}
