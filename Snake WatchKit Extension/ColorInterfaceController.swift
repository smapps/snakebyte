//
//  ColorInterfaceController.swift
//  SnakeByte
//
//  Created by Sameh Mabrouk on 5/27/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

import WatchKit

class ColorInterfaceController: SnakeColorInterfaceController {


    override func willActivate() {
        print("willActivate");
        // This method is called when watch view controller is about to be visible to user
        super.willActivate()
        
    }
    
    override func didDeactivate() {
        print("didDeactivate, ColorInterface");
        
//        NSUserDefaults.standardUserDefaults().setObject("red", forKey: "Color")
        // This method is called when watch view controller is no longer visible
        super.didDeactivate()
    }
}
