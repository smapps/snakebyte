//
//  MainViewController.m
//  WatchSnake
//
//  Created by Sameh Mabrouk on 4/23/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

#import "MainViewController.h"
#import "UIImage+ImageEffects.h"

#import <GameKit/GameKit.h>

#import "Reachability.h"

@import GoogleMobileAds;

//default blur settings
#define DEFAULT_BLUR_RADIUS 14
#define DEFAULT_BLUR_TINT_COLOR [UIColor colorWithWhite:0 alpha:.3]
#define DEFAULT_BLUR_DELTA_FACTOR 1.4


#define Snake_y 100
#define Snake_x 230

@interface MainViewController ()<UIActionSheetDelegate,GKGameCenterControllerDelegate>
{
    
    NSUserDefaults *mySharedDefaults;
    
}
@end

@implementation MainViewController

- (void)viewWillAppear:(BOOL)animated
{
    [[UIApplication sharedApplication] setStatusBarHidden:YES withAnimation:UIStatusBarAnimationNone];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [self authenticateLocalPlayer];
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    self.bannerView.adUnitID = @"ca-app-pub-1824327017795732/7310206606";
    self.bannerView.rootViewController = self;
    [self.bannerView loadRequest:[GADRequest request]];
    
    mySharedDefaults = [[NSUserDefaults alloc] initWithSuiteName:@"group.com.smapps.SnakeByte"];
    NSLog(@"High Score %li",(long)[mySharedDefaults integerForKey:@"HighScore"]);
    
    [self.scoreLabel setText:[@"Highscore: " stringByAppendingString:[NSString stringWithFormat:@"%li",(long)[mySharedDefaults integerForKey:@"HighScore"]]]];
    
    self.navigationController.navigationBarHidden=YES;
    
    [self.bgImageView setImage:[self imageWithColor:[UIColor colorWithRed:51.0/255 green:51.0/255 blue:51.0/255 alpha:1.0] andSize:CGSizeMake(self.view.frame.size.width, self.view.frame.size.height)]];
}

- (UIImage *)imageWithColor:(UIColor *)color andSize:(CGSize)size

{
    CGRect rect = CGRectMake(0.0f, 0.0f, size.width, size.height);
    
    //introduce high quality output image.
    UIGraphicsBeginImageContextWithOptions(rect.size, YES, 0.0f);
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    
    CGContextSetFillColorWithColor(context, [color CGColor]);
    CGContextFillRect(context, rect);
    
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextFillRect(context, CGRectMake(200, 60, 20, 20));
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextFillRect(context, CGRectMake(230, 400, 20, 20));
    CGContextSetFillColorWithColor(context, [UIColor greenColor].CGColor);
    CGContextFillRect(context, CGRectMake(60, 80, 20, 20));
    CGContextSetFillColorWithColor(context, [UIColor whiteColor].CGColor);
    
    int snakeY = 100;
    int snakeX=230;
    
    for (int i=0; i<8; i++) {
        NSLog(@"snakeX %i",snakeY);
        CGContextFillRect(context, CGRectMake(Snake_x, snakeY, 20, 20));
        snakeY+=20;
    }
    for (int j=0; j<7; j++) {
        CGContextFillRect(context, CGRectMake(snakeX, snakeY, 20, 20));
        snakeX-=20;
    }
    for (int j=0; j<13; j++) {
        CGContextFillRect(context, CGRectMake(snakeX, snakeY, 20, 20));
        snakeY+=20;
    }
    
    for (int j=0; j<4; j++) {
        CGContextFillRect(context, CGRectMake(snakeX, snakeY, 20, 20));
        snakeX+=20;
    }
    
    
    
    //    CGContextFillRect(context, CGRectMake(60, 120, 20, 20));
    //    CGContextFillRect(context, CGRectMake(60, 140, 20, 20));
    //    CGContextFillRect(context, CGRectMake(60, 160, 20, 20));
    //    CGContextFillRect(context, CGRectMake(60, 180, 20, 20));
    //    CGContextFillRect(context, CGRectMake(60, 200, 20, 20));
    
    
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    
    UIGraphicsEndImageContext();
    
    image = [image applyBlurWithRadius:DEFAULT_BLUR_RADIUS tintColor:[UIColor colorWithWhite:0 alpha:.5] saturationDeltaFactor:DEFAULT_BLUR_DELTA_FACTOR maskImage:nil];
    
    return image;
}

/**
 Check connectivity to the internet or not using Rechability Class.
 */
-(BOOL)connected{
    
    Reachability *internetReach = [Reachability reachabilityForInternetConnection];
    [internetReach startNotifier];
    
    NetworkStatus netStatus = [internetReach currentReachabilityStatus];
    
    switch (netStatus)
    {
        case ReachableViaWWAN:
        case ReachableViaWiFi:
        {
            return YES;
            break;
        }
        case NotReachable:
        {
            return NO;
            break;
        }
    }
    return NO;
}
#pragma mark - Game Center
-(void)authenticateLocalPlayer{
    NSLog(@"authenticateLocalPlayer");
    if ([self connected]) {
        GKLocalPlayer *localPlayer = [GKLocalPlayer localPlayer];
        
        localPlayer.authenticateHandler = ^(UIViewController *viewController, NSError *error){
            if (viewController != nil) {
                NSLog(@"presentViewController");
                //                [self presentViewController:viewController animated:YES completion:nil];
                self.GameCenterAuthVC=viewController;
                
            }
            else{
                if ([GKLocalPlayer localPlayer].authenticated) {
                    NSLog(@"user Authonticated");
                    
                    _gameCenterEnabled = YES;
                    
                    // Get the default leaderboard identifier.
                    [[GKLocalPlayer localPlayer] loadDefaultLeaderboardIdentifierWithCompletionHandler:^(NSString *leaderboardIdentifier, NSError *error) {
                        
                        if (error != nil) {
                            NSLog(@"Error...%@", [error localizedDescription]);
                        }
                        else{
                            _leaderboardIdentifier = leaderboardIdentifier;
                            NSLog(@"leaderboardIdentifier %@",leaderboardIdentifier);
                        }
                    }];
                }
                
                else{
                    NSLog(@"user canceled login");
                    _gameCenterEnabled = NO;
                }
            }
        };
        
        
    }
    else{
        
        //No Netwrok.
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"You're offline" message:@"Please connect to the internet and try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
    
    
}
-(void)reportScore{
    if ([self connected]) {
        
        NSLog(@"leaderboardIdentifier %@",_leaderboardIdentifier);
        if (_leaderboardIdentifier) {
            GKScore *score = [[GKScore alloc] initWithLeaderboardIdentifier:_leaderboardIdentifier];
            NSLog(@"High Score %li",(long)[mySharedDefaults integerForKey:@"HighScore"]);
            score.value = 35;
            
            [GKScore reportScores:@[score] withCompletionHandler:^(NSError *error) {
                if (error != nil) {
                    NSLog(@"%@", [error localizedDescription]);
                }
                else{
                    
                    UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"Highscore Submitted!" message:nil delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
                    [alert show];
                }
            }];

        }
        
    }
    else{
        
        //No Netwrok.
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"You're offline" message:@"Please connect to the internet and try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard{
    if ([self connected]) {
        GKGameCenterViewController *gcViewController = [[GKGameCenterViewController alloc] init];
        
        gcViewController.gameCenterDelegate = self;
        
        if (shouldShowLeaderboard) {
            gcViewController.viewState = GKGameCenterViewControllerStateLeaderboards;
            gcViewController.leaderboardIdentifier = _leaderboardIdentifier;
        }
        else{
            gcViewController.viewState = GKGameCenterViewControllerStateAchievements;
        }
        
        [self presentViewController:gcViewController animated:YES completion:nil];
    }
    else{
        //No Netwrok.
        UIAlertView *alert=[[UIAlertView alloc] initWithTitle:@"You're offline" message:@"Please connect to the internet and try again" delegate:self cancelButtonTitle:nil otherButtonTitles:@"Ok", nil];
        [alert show];
    }
}

- (IBAction)openGameCenterAction:(id)sender {
    
    if (_gameCenterEnabled==NO&&self.GameCenterAuthVC) {
        [self presentViewController:self.GameCenterAuthVC animated:YES completion:nil];
    }
    else if (_gameCenterEnabled){
        
        [self showGameCenterActionSheet];
    }
    
}

- (IBAction)shareAction:(id)sender {
    NSLog(@"shareAction");
    UIActivityViewController *activityVC = [[UIActivityViewController alloc] initWithActivityItems:[NSArray arrayWithObjects:@"Check out Snake Byte game for Apple Watch, Experience Game Playing on your Wrist!", nil] applicationActivities:nil];
    //activityVC.excludedActivityTypes = @[ UIActivityTypeMessage ,UIActivityTypeMail,UIActivityTypePostToTwitter];
    // check if new API supported
    if ([activityVC respondsToSelector:@selector(completionWithItemsHandler)]) {
        activityVC.completionWithItemsHandler = ^(NSString *activityType, BOOL completed, NSArray *returnedItems, NSError *activityError) {
            // When completed flag is YES, user performed specific activity
            
            NSString *ServiceMsg = nil;
            if ( [activityType isEqualToString:UIActivityTypeMail] )
                ServiceMsg = @"Mail sent";
            
            if ( [activityType isEqualToString:UIActivityTypePostToTwitter] )
                ServiceMsg = @"Posted on twitter, ok!";
            
            if ( [activityType isEqualToString:UIActivityTypePostToFacebook] )
                ServiceMsg = @"Posted on facebook, ok!";
            
            if ( completed )
            {
                UIAlertView *Alert = [[UIAlertView alloc] initWithTitle:ServiceMsg message:@"" delegate:nil cancelButtonTitle:@"ok" otherButtonTitles:nil];
                [Alert show];
                
            }
            else
            {
                // didn't succeed.
            }
            
        };
    } else {
        activityVC.completionHandler = ^(NSString *activityType, BOOL completed) {
            // When completed flag is YES, user performed specific activity
        };
    }
    
    [self presentViewController:activityVC animated:YES completion:nil];
    
}

- (IBAction)rateAction:(id)sender {
    NSLog(@"rateAction");
    NSString *str = @"itms-apps://ax.itunes.apple.com/WebObjects/MZStore.woa";
    str = [NSString stringWithFormat:@"%@/wa/viewContentsUserReviews?", str];
    str = [NSString stringWithFormat:@"%@type=Purple+Software&id=", str];
    
    // Here is the app id from itunesconnect
    str = [NSString stringWithFormat:@"%@991617826", str];
    
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:str]];
}

#pragma mark - GKGameCenterControllerDelegate
-(void)gameCenterViewControllerDidFinish:(GKGameCenterViewController *)gameCenterViewController
{
    [gameCenterViewController dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark - showGameCenterActionSheet
- (void)showGameCenterActionSheet {
    UIActionSheet *actionSheet = [[UIActionSheet alloc] initWithTitle:nil
                                                             delegate:self
                                                    cancelButtonTitle:@"Cancel"
                                               destructiveButtonTitle:nil
                                                    otherButtonTitles:@"Submit Score",@"Show Leaderboards" ,nil];
    
    [actionSheet showInView:self.view];
}
#pragma mark - UIACtionSheetDelegate
- (void)actionSheet:(UIActionSheet *)actionSheet clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if (buttonIndex==0) {
        [self reportScore];
    }
    else if (buttonIndex==1){
        
        [self showLeaderboardAndAchievements:YES];
    }
}

/*
 #pragma mark - Navigation
 
 // In a storyboard-based application, you will often want to do a little preparation before navigation
 - (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
 // Get the new view controller using [segue destinationViewController].
 // Pass the selected object to the new view controller.
 }
 */

@end
