//
//  BaseViewController.m
//  Snake
//
//  Created by Sameh Mabrouk on 5/29/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

#import "BaseViewController.h"
#import "HowToViewController.h"
#import "MainViewController.h"

@interface BaseViewController ()

@end

@implementation BaseViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view.
    
    UIStoryboard *storyboard = [UIStoryboard storyboardWithName:@"Main" bundle:nil];
    HowToViewController *howToVC = [storyboard instantiateViewControllerWithIdentifier:@"HowToVC"];
    MainViewController *mainVC = [storyboard instantiateViewControllerWithIdentifier:@"MainVCID"];
    //    TestViewController *testVC =[[TestViewController alloc] init];
    
    self.circleVC = [[KLCircleViewController alloc] initWithCenterViewController: mainVC
                                                              leftViewController: nil
                                                             rightViewController: nil
                                                            bottomViewController: howToVC];
    
    
    [self.circleVC setWillTransitionState: ^(KLCircleViewController* circleViewController, KLCircleState fromState,
                                             KLCircleState toState) {
        NSLog(@"State before change: %d", circleViewController.state);
    }];
    [self.circleVC setDidTransitionState:^(KLCircleViewController* circleViewController, KLCircleState fromState,
                                           KLCircleState toState) {
        NSLog(@"State after change: %d", circleViewController.state);
    }];
    
    

}

-(void) viewDidAppear:(BOOL)animated {
    [self presentViewController: self.circleVC animated:YES completion:nil];
    
}
- (IBAction)didPressCenter:(id)sender {
    [self.circleVC setState:KLCircleStateCenter
                   animated:YES];
}

- (IBAction)didPressLeftButton:(id)sender {
    [self.circleVC setState:KLCircleStateLeft
                   animated:YES];
}

- (IBAction)didPressRightButton:(id)sender {
    [self.circleVC setState:KLCircleStateRight
                   animated:YES];
}

- (IBAction)didPressDownButton:(id)sender {
    [self.circleVC setState: KLCircleStateBottom
                   animated:YES];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
