//
//  BaseViewController.h
//  Snake
//
//  Created by Sameh Mabrouk on 5/29/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "KLCircleViewController.h"

@interface BaseViewController : UIViewController
@property (nonatomic, strong) KLCircleViewController* circleVC;

@end
