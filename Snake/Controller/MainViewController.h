//
//  MainViewController.h
//  WatchSnake
//
//  Created by Sameh Mabrouk on 4/23/15.
//  Copyright (c) 2015 smapps. All rights reserved.
//

#import <UIKit/UIKit.h>
@import GoogleMobileAds;

@interface MainViewController : UIViewController
@property(nonatomic,retain) UIImageView *iconImageView;
@property(nonatomic,weak)IBOutlet UIImageView *bgImageView;
@property (weak, nonatomic) IBOutlet GADBannerView *bannerView;
@property(nonatomic)BOOL gameCenterEnabled;
@property(nonatomic,strong)NSString *leaderboardIdentifier;
@property(nonatomic,strong)UIViewController *GameCenterAuthVC;

@property (weak, nonatomic) IBOutlet UILabel *scoreLabel;

-(void)authenticateLocalPlayer;
-(void)reportScore;
-(void)showLeaderboardAndAchievements:(BOOL)shouldShowLeaderboard;

- (IBAction)openGameCenterAction:(id)sender;
- (IBAction)shareAction:(id)sender;
- (IBAction)rateAction:(id)sender;

@end
